import User,{UserModel} from '../model/user.model';
import { Types } from 'mongoose';
export default class UserRepo{
    public static findById(id:Types.ObjectId): Promise<any> {
        return UserModel.findOne({ _id: id})
        .lean<User>()
        .exec();
    }
    public static create(user:User): Promise<any> {
        return UserModel.findOneAndUpdate({email:user.email},{email:user.email,name:user.name,photo:user.photo},{upsert:true})
        .lean<User>()
        .exec();
    }
}