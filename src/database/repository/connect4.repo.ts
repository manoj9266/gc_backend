import IConnect4,{Connect4Model, IMove} from '../model/connect4.model';
import { Types } from 'mongoose';
import { stat } from 'fs';
export default class Connect4Repo{
    public static findById(id:Types.ObjectId): Promise<any> {
        return Connect4Model.findOne({ _id: id})
        .lean<IConnect4>()
        .exec();
    }
    public static update(id:Types.ObjectId,status:number): Promise<any> {
        return Connect4Model.updateOne({ _id: id},{$set:{game_status:status}})
        .lean<IConnect4>()
        .exec();
    }
    public static create(): Promise<any> {
         let fourmatch = new Connect4Model({game_status:1});
        return fourmatch.save();
    }
    public static addMove(_id:Types.ObjectId,move:IMove): Promise<any> {
        return Connect4Model.findOneAndUpdate({_id:_id},{$push:{moves:move}}).lean().exec();    
   }
}