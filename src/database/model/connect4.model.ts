import {model,Schema,Document} from 'mongoose';
export const DOCUMENT_NAME = 'connect4';
export default interface IConnect4 extends Document{  
  moves:[IMove],  
  game_status:number,
  created_at:Date
}
export  interface IMove{  
  player:number,  
  row:number,
  col:number
}
let schema :Schema = new Schema({   
   moves:{type:[{player:{type:Number},row:{type:Number},col:{type:Number}}]},
   game_status:{type:Number,default:1},
   created_at:{type:Date,default:Date.now}
})
export const Connect4Model = model<IConnect4>(DOCUMENT_NAME,schema)
