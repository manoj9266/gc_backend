import {model,Schema,Document} from 'mongoose';
export const DOCUMENT_NAME = 'user';
export default interface IUser extends Document{  
  name:string,  
  email:string,
  photo?:string      
}
let schema :Schema = new Schema({   
   name:{type:String,required:true},   
   email:{type:String,unique:true,required:true},
   photo:{type:String},   
})
export const UserModel = model<IUser>(DOCUMENT_NAME,schema)
