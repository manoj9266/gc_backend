import mongoose from 'mongoose'
import * as dotenv from 'dotenv';
dotenv.config();
console.log("db="+process.env.MONGO_DB_URL);
mongoose.connect(process.env.MONGO_DB_URL+"",{useCreateIndex: true,useNewUrlParser:true, useUnifiedTopology: true,useFindAndModify: false},(err)=>{

   if(!err){ console.log("MongoDB  connection Succeeded...");}
   else {console.log("error in db creation"); console.log(err)}
});