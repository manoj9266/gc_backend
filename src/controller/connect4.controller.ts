import express,{Request,Response} from 'express'
import Controller from '../interfaces/controller.interface';
import Connect4Repo from '../database/repository/connect4.repo'
import  { IMove } from '../database/model/connect4.model'
import { isValidObjectId ,Types} from 'mongoose';
const max_rows = 6;
const max_cols = 7;
class UserController implements Controller{
    public router = express.Router();
    public path = "/connect4";
    constructor(){
        this.initializeRoutes();
      }
      private initializeRoutes(){   
        
        this.router.get('/start',this.startGame);
 
        this.router.get('/move',this.move);
       }
       private startGame = async (req:Request,res:Response)=>{
         try{
        
         let fourMatch = await Connect4Repo.create();
              return res.json({status:"READY",token:fourMatch._id});
        }catch(err){
          console.log(err);
          return res.status(400).send("something went wrong!!");
        }
       }
       private move = async (req:Request,res:Response)=>{
        try{
          const params = req.query;
          let token = params.token+"";
          console.log(token);
          const col = params.col && (+params.col>=0)?+params.col:-1;

          if(!token || !isValidObjectId(token)){
              return res.status(400).send("Invalid token");
          }else if(col<0 || col>=max_cols){
            return res.send({status:false,message:"Invalid col"});
          }            
          // validating user id
          
          let game = await Connect4Repo.findById(Types.ObjectId(token));
          let moves = game.moves;
          const turn = moves.length%2;
          
          let row =-1;
          moves.forEach((element:IMove) => {
              if(element.col==col){
                  row = Math.max(row,element.row);
              }
          });
          row++;
          if(row>=max_rows){
             return res.send({status:false,message:"Invalid"});
          }
          
          let move :IMove= {row:row,col:col,player:turn};
          //await game.moves.push(move).save();
          
          let user_moves = moves.filter((item:IMove)=> item.player==turn);
          user_moves.push(move);
          let is_win = await this.checkForWin(row,col,user_moves);
          console.log(user_moves);
          if(is_win){
              await Connect4Repo.update(Types.ObjectId(token),2);
          }else{
            await Connect4Repo.addMove(Types.ObjectId(token),move) ;         
          }
          let game_status = is_win?"Player "+turn+" Won":"running"
          res.send({status:true,message:"Valid",game_status:game_status});
          }catch(err){
            console.log(err);
            res.status(400).send("something went wrong!!");
          }
       }


        async checkForWin(row:number,col:number,moves:[IMove]){
            let rw = await this.isRowComp(row,col,moves)  ;
            let cw = await this.isColComp(row,col,moves);
            let dw = await this.isDiagComp(row,col,moves);
            console.log("rw="+rw+" cw="+cw+" dw="+dw);
        if(rw || cw || dw){
          return true;
        }
        return false;
     }
      compareCol(a:IMove, b:IMove) {
        if (a.col > b.col) return 1;
        if (b.col > a.col) return -1;
      
        return 0;
      }
      compareRow(a:IMove, b:IMove) {
        if (a.row > b.row) return 1;
        if (b.row > a.row) return -1;      
        return 0;
      }      
     async isRowComp(row:number,col:number,moves:[IMove]){
        let totalCount = 0;
        let rowMoves = moves.filter((item)=>item.row==row);
        rowMoves.sort(this.compareCol);
        let currentCol = -1;
        let win = false;
        rowMoves.forEach(item=>{
             if(currentCol==-1 || item.col!=currentCol+1){
                 currentCol = item.col;
                 totalCount=1;
             }else {
                currentCol = item.col;
                totalCount++;
                if(totalCount>=4){
                    win =true;
                }
             }
             
        })
        return win;
      }
      async isColComp(row:number,col:number,moves:[IMove]){
        let totalCount = 0;
        let colMoves = moves.filter((item)=>item.col==col);
        console.log("colMoves");
        console.log(colMoves);
        colMoves.sort(this.compareRow);
        let currentRow = -1;
        let win = false;
        colMoves.forEach(item=>{
             if(currentRow==-1 || item.row!=currentRow+1){                 
                currentRow = +item.row;
                 totalCount=1;
             }else {
                 
                currentRow = +item.row;
                totalCount++;
                console.log("else total_count="+totalCount);
                if(totalCount>=4){
                    win = true;
                }
             }
             
        });
        return win;
      }
      async isDiagComp(row:number,col:number,moves:[IMove]){
        let totalCount = 0;
        let dMoves = moves.filter((item)=>item.col-col==item.row-row);
        dMoves.sort(this.compareRow);
        let currentRow = -1;
        let currentCol = -1;
        let win = false;
        dMoves.forEach(item=>{
             if(currentRow==-1 || (item.col!=currentCol+1)|| (item.row!=currentRow+1)){
                currentRow = +item.row;
                currentCol = +item.col;
                 totalCount=1;
             }else {
                currentRow = +item.row;
                currentCol = +item.col;
                totalCount++;
                if(totalCount>=4){
                    win = true;
                }
             }
             
        });
        return win;
    }
      
}
export default UserController;