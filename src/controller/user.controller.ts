import express,{Request,Response} from 'express'
import Controller from '../interfaces/controller.interface';
import UserRepo from '../database/repository/user.repo'
import IUser from '../database/model/user.model'
import { isValidObjectId ,Types} from 'mongoose';
class UserController implements Controller{
    public router = express.Router();
    public path = "/user";
    constructor(){
        this.initializeRoutes();
      }
      private initializeRoutes(){
        
        this.router.get('/',this.getUser);
 
        this.router.post('/',this.register);
       }
       private getUser = async (req:Request,res:Response)=>{
         try{
        const params = req.query;            
        // validating user id
        if(!params.user_id || isValidObjectId(params.user_id)===false){
          return res.json({status:false,message:"invalid user id"});
         }
              let user = await UserRepo.findById(Types.ObjectId(params.user_id+""));
              return res.json(user);
        }catch(err){
          console.log(err);
          return res.status(400).send("something went wrong!!");
        }
       }
       private register = async (req:Request,res:Response)=>{
        try{
          const params = req.body;            
          // validating user id
          const user:IUser = await UserRepo.create({            
            name:params.name,
            email:params.email,
            photo:params.photo                    
          } as IUser);
          console.log(user);
          res.json(user);

          }catch(err){
            console.log(err);
            res.status(400).send("something went wrong!!");
          }
       }
}
export default UserController;